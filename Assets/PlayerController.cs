﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PlayerController : NetworkBehaviour
{

    [SerializeField]
    GameObject cubePrefab;


    public void SpawnCube(Vector3 position)
    {
        if (isLocalPlayer)
        {
            CmdSpawnCube(position);
        }
    }

    [Command]
    void CmdSpawnCube(Vector3 position)
    {
        GameObject newCube = (GameObject)Instantiate(cubePrefab, position, Quaternion.identity);

        GameObject stage = GameObject.FindGameObjectWithTag("Stage");

        newCube.transform.SetParent(stage.transform);

        NetworkServer.Spawn(newCube);
    }
}
