﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class CubeSideController : NetworkBehaviour
{
    [SerializeField]
    Color originalColor;

    [SerializeField]
    Color mouseOverColor;

    [SerializeField]
    GameObject cube;

    [SerializeField]
    bool active = true;

    Material material;

    bool isFloor = false;

    public void SetFloor(bool floor)
    {
        isFloor = floor;
    }

    public bool GetFloor()
    {
        return isFloor;
    }

    void Awake()
    {
        material = GetComponent<Renderer>().material;
        material.color = originalColor;
    }

    void OnMouseEnter()
    {
        if (active)
        {
            material.color = mouseOverColor;
        }
    }

    void OnMouseExit()
    {
        if (active)
        {
            material.color = originalColor;
        }
    }

    void OnMouseUp()
    {
        if (active)
        {
            CmdMakeCube();
        }
    }

    
    public void CmdMakeCube()
    {
        

        Vector3 newCubePosition = transform.position + transform.rotation * Vector3.back * (transform.localScale.x / 2);

        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        foreach (GameObject player in players)
        {
            PlayerController pc = player.GetComponent<PlayerController>();
            pc.SpawnCube(newCubePosition);
        }

        //GameObject newCube = (GameObject)Instantiate(cube, newCubePosition, Quaternion.identity);

        //newCube.transform.SetParent(transform.parent.parent);
    }

}
