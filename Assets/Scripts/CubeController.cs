﻿using UnityEngine;
using System.Collections;

public class CubeController : MonoBehaviour {


    [SerializeField]
    GameObject cubeSidePrefab;

    public float sizeOfEachSide = 2f;

	// Use this for initialization
	void Start () {

        Vector3 sizeVector = new Vector3(sizeOfEachSide, sizeOfEachSide, sizeOfEachSide);

        GetComponent<BoxCollider>().size = sizeVector;

        Vector3[] sides = new Vector3[6]
        {
            Vector3.up,
            Vector3.down,
            Vector3.left,
            Vector3.right,
            Vector3.forward,
            Vector3.back
        };

        for (int i=0; i < sides.Length; i++)
        {
            GameObject newSide = (GameObject)Instantiate(cubeSidePrefab, this.transform);
            newSide.transform.localScale = sizeVector;
            newSide.transform.localPosition = sides[i] * (sizeOfEachSide / 2);
            newSide.transform.LookAt(this.transform.position + sides[i] * -2);

            if (i == 0)
            {
                newSide.layer = LayerMask.NameToLayer("Floor");
            }

        }
	}
}
