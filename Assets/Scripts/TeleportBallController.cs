﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]

public class TeleportBallController : MonoBehaviour
{

    [SerializeField]
    GameObject theBallModel;

    [SerializeField]
    float degreesPerSecond = 30f;

    [SerializeField]
    float rotationAcceleration = 1f;

    [SerializeField]
    Transform restPosition;

    [SerializeField]
    Transform activePosition;

    [SerializeField]
    float floatSpeed = 1;

    float currentDegreesPerSecond = 0f;

    bool active = false;
    bool free = false;

    GameObject currentCubeSide;

    // Update is called once per frame
    void Update()
    {

        if (active && !free)
        {
            currentDegreesPerSecond = Mathf.Lerp(currentDegreesPerSecond, degreesPerSecond, rotationAcceleration * Time.deltaTime);

            transform.position = Vector3.Lerp(transform.position, activePosition.position, floatSpeed * Time.deltaTime);
        }
        else if (!active && !free)
        {
            currentDegreesPerSecond = Mathf.Lerp(currentDegreesPerSecond, 0, rotationAcceleration * Time.deltaTime);
            transform.position = Vector3.Lerp(transform.position, restPosition.position, floatSpeed * Time.deltaTime);
        }

        if (currentDegreesPerSecond > 0)
        {
            theBallModel.transform.Rotate(Vector3.up, currentDegreesPerSecond * Time.deltaTime);
        }

    }

    void OnTriggerEnter(Collider coll)
    {
        Debug.Log(coll.gameObject.layer);

        if (coll.gameObject.layer == LayerMask.NameToLayer("Floor"))
        {
            Stop();
            currentCubeSide = coll.gameObject;
        }
    }

    public void Stop()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        this.transform.rotation = Quaternion.identity;
    }

    public Vector3 GetTeleportTarget()
    {
        Stop();
        Vector3 newpos = transform.position;
        newpos.y = currentCubeSide.transform.position.y;

        return newpos;
    }

    public bool IsStopped()
    {
        Rigidbody rb = GetComponent<Rigidbody>();

        return rb.velocity == Vector3.zero && rb.angularVelocity == Vector3.zero;
    }

    /* ------------------------------------------------------------------------
     * ACTIVATION & DEACTIVATION
     * ----------------------------------------------------------------------*/

    public void Activate()
    {
        active = true;
    }

    public void Deactivate()
    {
        active = false;
    }

    public bool IsActive()
    {
        return active;
    }

    /* ------------------------------------------------------------------------
    * RELEASE & CAPTURE
    * ----------------------------------------------------------------------*/

    public void Release()
    {
        free = true;
        transform.SetParent(null);
        GetComponent<Rigidbody>().isKinematic = false;
    }

    public void Capture(Transform parent)
    {
        free = false;
        Deactivate();
        transform.position = restPosition.position;
        transform.SetParent(parent);

        Rigidbody rigidbody = GetComponent<Rigidbody>();

        rigidbody.isKinematic = true;

        Stop();
    }

    public bool IsFree()
    {
        return free;
    }
}
