﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(SteamVR_TrackedObject))]

public class TeleportBallBaseController : MonoBehaviour
{

    [SerializeField]
    TeleportBallController ball;

    [SerializeField]
    GameObject VirtualRealityRoom;


    // Markers
    [SerializeField]
    GameObject trackpadMarker;

    [SerializeField]
    GameObject gripMarker;


    SteamVR_TrackedObject trackedObj;
    SteamVR_Controller.Device device;

    bool hasLoaded = false;

    void Start()
    {

        trackedObj = GetComponent<SteamVR_TrackedObject>();

        hasLoaded = true;

    }

    void FixedUpdate()
    {

        if (hasLoaded)
        {

            device = SteamVR_Controller.Input((int)trackedObj.index);

            if (device.GetTouch(SteamVR_Controller.ButtonMask.Trigger))
            {
                ball.Activate();
            }
            else if (!ball.IsFree() && ball.IsActive())
            {
                TossBall();
            }
            else
            {
                ball.Deactivate();
            }

            if (device.GetTouch(SteamVR_Controller.ButtonMask.Grip) && ball.IsFree())
            {
                ReturnBallToHolder();
            }

            if (device.GetPress(SteamVR_Controller.ButtonMask.Touchpad) && ball.IsFree() && ball.IsStopped())
            {
                Vector3 teleportTargetPosition = ball.GetTeleportTarget();

                VirtualRealityRoom.transform.position = teleportTargetPosition;

                ReturnBallToHolder();
            }


            CheckMarkers();
        }

    }

    void CheckMarkers()
    {
        if (ball.IsFree() && ball.IsStopped() && !trackpadMarker.activeSelf)
        {
            trackpadMarker.SetActive(true);
        }
        else if ((!ball.IsFree() || !ball.IsStopped()) && trackpadMarker.activeSelf)
        {
            trackpadMarker.SetActive(false);
        }
    }

    void TossBall()
    {
        // Release the ball
        ball.Release();

        // Get the ball's rigidbody
        Rigidbody rigidbody = ball.gameObject.GetComponent<Rigidbody>();


        // Apply velocities

        Transform origin = trackedObj.origin ? trackedObj.origin : trackedObj.transform.parent;

        if (origin != null)
        {
            // Transform from local to world space
            rigidbody.velocity = origin.TransformVector(device.velocity);
            rigidbody.angularVelocity = origin.TransformVector(device.angularVelocity);
        }
        else
        {
            rigidbody.velocity = device.velocity;
            rigidbody.angularVelocity = device.angularVelocity;
        }


    }

    void ReturnBallToHolder()
    {
        ball.Capture(this.transform);
    }
}
