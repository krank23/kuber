﻿using UnityEngine;
using System.Collections;

public class NonVRCameraController : MonoBehaviour
{
    [SerializeField]
    Transform stage;

    [SerializeField]
    float speed = 2;

    /*[SerializeField]
    float minVerticalRotation = -0.4f;

    [SerializeField]
    float maxVerticalRotation = 0.4f;*/

    [SerializeField]
    float zoomSpeed = 10;

    [SerializeField]
    float zoomSmooth = 5;

    [SerializeField]
    float maxZoomDistance = 50f;

    [SerializeField]
    float minZoomDistance = 25f;


    GameObject selectedBoxSide;


    float distance;

    // Use this for initialization
    void Start()
    {
        transform.LookAt(stage);
        distance = Vector3.Distance(this.transform.position, stage.position);
    }

    // Update is called once per frame
    void Update()
    {

        // Vertical rotation

        /*float moveY = Input.GetAxis("Vertical");
        float rotationAngle = moveY * speed * Time.deltaTime;
        if (distance - Mathf.Abs(stage.position.y - transform.position.y) > 1)
        {
            transform.RotateAround(stage.position, transform.rotation * Vector3.left, rotationAngle);
        }*/

        orbitControl();
        zoomControl();

    }

    void orbitControl()
    {
        // Get axis value from controls
        float moveX = Input.GetAxis("CamHorizontal");
        // Point camera at stage's center and rotate around it
        transform.LookAt(stage);
        transform.RotateAround(stage.position, Vector3.up, moveX * speed * Time.deltaTime);
    }

    void zoomControl()
    {
        // Get axis value
        float zoom = Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;

        // Apply zoom to distance, and clamp it
        distance -= zoom * zoomSpeed;
        distance = Mathf.Clamp(distance, minZoomDistance, maxZoomDistance);

        /* Create a new vector using the distance as z, rotate the vector according to the camera's
         * current rotation, and offset the stage's current position */
        Vector3 position = transform.rotation * new Vector3(0, 0, -distance) + stage.position;

        // Lerp to the new position
        transform.position = Vector3.Lerp(transform.position, position, zoomSmooth * Time.deltaTime);
    }

}
