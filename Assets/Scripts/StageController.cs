﻿using UnityEngine;
using System.Collections;

public class StageController : MonoBehaviour {

    [SerializeField]
    int stageSize = 5;

    [SerializeField]
    GameObject cubePrefab;

    float cubeSize = 2;

    // Use this for initialization
    void Start () {
        Reset();
	}

    public void Reset()
    {
        // Get the cube size
        CubeController cc = cubePrefab.GetComponent<CubeController>();
        if (cc)
        {
            cubeSize = cc.sizeOfEachSide;
        }


        // Destroy all child objects

        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }

        // Create new cubes

        // Columns
        for (int x = -stageSize; x <= stageSize; x++)
        {
            // Rows
            for (int z = -stageSize; z <= stageSize; z++)
            {
                // Create vector from stated cube size
                Vector3 pos = transform.position + new Vector3(x * cubeSize, 0, z * cubeSize);

                // Instantiate new cube and make it a child of the stage
                GameObject newCube = (GameObject)Instantiate(cubePrefab, pos, Quaternion.identity);
                newCube.transform.SetParent(this.transform);
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        for (int x = -stageSize; x <= stageSize; x++)
        {
            for (int z = -stageSize; z <= stageSize; z++)
            {
                Vector3 pos = new Vector3(x * cubeSize, 0, z * cubeSize);

                Gizmos.DrawWireCube(pos, new Vector3(cubeSize, cubeSize, cubeSize));

            }
        }
    }
}
